FROM node

WORKDIR /myapp

COPY . .

EXPOSE 4000

CMD node server.js