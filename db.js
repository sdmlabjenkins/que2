const express = require('express')
const mysql = require('mysql')

function connect(){
    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'car_db',
        port : 3306
    })

    connection.connect()
    return connection
}

module.exports = {
    connect,
}