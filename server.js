const express = require('express')
const db = require('./db')
const utils = require('./utils')

const app = express()
app.use(express.json())

app.get('/',(request,response)=>{
    const statement = `select car_id,car_name,company_name,car_price 
    from car;`
    const connection = db.connect()
    connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

app.post('/add',(request,response)=>{
    const {car_id,car_name,company_name,car_price} = request.body
    const statement = `insert into car (car_id, car_name, company_name, car_price) 
    values ('${car_id}','${car_name}','${company_name}','${car_price}')
    `
    const connection = db.connect()
    connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

app.put('/update/:car_id',(request,response)=>{
    const {car_id} = request.params
    const {car_name,company_name,car_price} = request.body

    const statement = `update car set 
            car_name = '${car_name}', 
            company_name = '${company_name}' ,
            car_price = '${car_price}'
    where car_id = '${car_id}'
    `
    const connection = db.connect()
    connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

app.delete('/delete/:car_id',(request,response)=>{
    const {car_id} = request.params

    const statement = `delete from car where car_id = '${car_id}' `
    const connection = db.connect()
    connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

app.listen (4000,'0.0.0.0',()=>{
    console.log('server started on port 4000')
})